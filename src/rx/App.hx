package rx;

class App {
    public var width = 1280;
    public var height = 720;
    public var state:State;

    public function new(s:State) {
        Rl.setConfigFlags(Rl.ConfigFlags.FLAG_VSYNC_HINT);
        Rl.initWindow(width, height, "");
        Rl.setExitKey(Rl.Keys.NULL);

        state = s;
        state.start();

        while(!Rl.windowShouldClose()) {
            state.update();

            Rl.beginDrawing();
            Rl.clearBackground(Rl.Colors.BLACK);
            state.render();
            Rl.endDrawing();
        }

        state.shutdown();

        Rl.closeWindow();
    }
}