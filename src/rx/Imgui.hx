package rx;

import Rl.MouseCursor;
import imgui.ImGui;

class Imgui {
    public var mouseCursorMap:Map<ImGuiMouseCursor, MouseCursor>;
    public var currentMouseCursor = ImGuiMouseCursor.COUNT;

    public function new() {
    }

    function newFrame() {
        var io = ImGui.getIO();

        io.displaySize.x = 1280;
        io.displaySize.y = 720;
        var width = io.displaySize.x; var height = io.displaySize.y;

        if(width > 0 && height > 0) {
            io.displayFramebufferScale = ImVec2.create(width / io.displaySize.x, height / io.displaySize.y);
        } else {
            io.displayFramebufferScale = ImVec2.create(1, 1);
        }

        io.deltaTime = Rl.getFrameTime();

        io.keyCtrl = Rl.isKeyDown(Rl.Keys.RIGHT_CONTROL) || Rl.isKeyDown(Rl.Keys.LEFT_CONTROL);
        io.keyShift = Rl.isKeyDown(Rl.Keys.RIGHT_SHIFT) || Rl.isKeyDown(Rl.Keys.LEFT_SHIFT);
        io.keyAlt = Rl.isKeyDown(Rl.Keys.RIGHT_ALT) || Rl.isKeyDown(Rl.Keys.LEFT_ALT);
        io.keySuper = Rl.isKeyDown(Rl.Keys.RIGHT_SUPER) || Rl.isKeyDown(Rl.Keys.LEFT_SUPER);

        if(io.wantSetMousePos) {
            Rl.setMousePosition(Std.int(io.mousePos.x), Std.int(io.mousePos.y));
        } else {
            io.mousePos.x = Rl.getMouseX();
            io.mousePos.y = Rl.getMouseY();
        }

        io.mouseDown[0] = Rl.isMouseButtonDown(Rl.MouseButton.LEFT);
        io.mouseDown[1] = Rl.isMouseButtonDown(Rl.MouseButton.RIGHT);
        io.mouseDown[2] = Rl.isMouseButtonDown(Rl.MouseButton.MIDDLE);

        if(Rl.getMouseWheelMove() > 0) {
            io.mouseWheel += 1;
        } else if(Rl.getMouseWheelMove() < 0) {
            io.mouseWheel -= 1;
        }

        if((io.configFlags & ImGuiConfigFlags.NoMouseCursorChange) == 0) {
            var imguiCursor = ImGui.getMouseCursor();
            if(imguiCursor != currentMouseCursor || io.mouseDrawCursor) {
                currentMouseCursor = imguiCursor;
                if(io.mouseDrawCursor || imguiCursor == ImGuiMouseCursor.None) {
                    Rl.hideCursor();
                }  else {
                    Rl.showCursor();

                    if((io.configFlags & ImGuiConfigFlags.NoMouseCursorChange) == 1) {
                        // TODO
                    }
                }
            }
        }
    }

    function imguiEvents() {
        var io = ImGui.getIO();
        // io.keysDown[]
    }
}