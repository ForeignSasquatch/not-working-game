package rx;

import echo.Body;

class Prop {
    public var x:Float;
    public var y:Float;
    public var tex:Rl.Texture;
    public var hitbox:Rl.Rectangle;
    public var selected:Bool = false;
    public var file:String;

    public static var id:Int = 1;
    public var pid:Int;
    
    public var body:Body;

    public function new(x:Float, y:Float, file:String) {
        this.x = x;
        this.y = y;
        this.file = file;
        tex = Rl.loadTexture(file);

        hitbox = Rl.Rectangle.create(x, y, tex.width, tex.height);
        pid = id;
        id++;

        body = new Body({
            x: this.x,
            y: this.y,
            kinematic: true,
            mass: 1,
            shape: {
                type: RECT,
                width: tex.width,
                height: tex.height,
                solid: true
            }
        });

        body.on_move = (x, y) -> {
            this.x = x;
            this.y = y;
        };
    }

    public function render() {
        hitbox.x = x;
        hitbox.y = y;
        // Rl.drawTexture(tex, Std.int(x), Std.int(y), Rl.Colors.WHITE);
        Rl.drawTextureV(tex, Rl.Vector2.create(x, y), Rl.Colors.WHITE);
        if(selected) Rl.drawRectangleLinesEx(hitbox, 2, Rl.Colors.RED);
        if(selected) Rl.drawText(Std.string(id), 0, 21, 20, Rl.Colors.WHITE);
    }

    public function delete() {
        Rl.unloadTexture(tex);
    }
}