package rx;

class State {
    public function start() {}
    public function update() {}
    public function render() {}
    public function shutdown() {}

    public function new() {}
}