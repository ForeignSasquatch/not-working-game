import echo.Echo;
import echo.World;
import rx.State;
import rx.Prop;

enum Props {
    NULL;
    TESTBLOCK;
}

typedef SaveObj = {
    public var x:Float;
    public var y:Float;
    public var file:String;
    public var id:Int;
};

typedef FileFormat = {
	props:Array<SaveObj>
}

class Editor extends State {
    public var prop:Props = NULL;
    public var props:Map<Int, Prop> = [];
    public var currentProp = "";
    var savef:String = "res/testl.json";
    var player:Player;
    var level:FileFormat;
    var world:World;

    override function start() {
        world = Echo.start({
            width: 1280,
            height: 720,
            gravity_y: 60,
        });

        level = haxe.Json.parse(sys.io.File.getContent("res/testl.json"));
        for(p in level.props) {
            var e = new Prop(p.x, p.y, p.file);
            props.set(e.pid, e);
            world.add(e.body);
        }

        player = new Player(0, 0);
        player.start();
    
        world.add(player.body);
    }

    var mousePos = Rl.getMousePosition();
    override function update() {
        world.step(1 / 60);

        player.update();

        mousePos = Rl.getMousePosition();

        for(p in props) {
            if(Rl.checkCollisionPointRec(Rl.getMousePosition(), p.hitbox) && Rl.isKeyPressed(Rl.Keys.E)) {
                p.selected = !p.selected;
            }
        }

        if(Rl.isKeyPressed(Rl.Keys.ONE)) {
            prop = TESTBLOCK;
        }

        if(Rl.isMouseButtonPressed(Rl.MouseButton.LEFT)) {
            switch(prop) {
                case TESTBLOCK:
                    var e = new Prop(mousePos.x, mousePos.y, "res/TestBlock.png");
                    props.set(e.pid, e);
                    world.add(e.body);
                default:
            }
        }

        for(p in props) {
            if(Rl.checkCollisionPointRec(Rl.getMousePosition(), p.hitbox) && Rl.isMouseButtonPressed(Rl.MouseButton.RIGHT)) {
                world.remove(p.body);
                p.body.dispose();
                props.remove(p.pid);
                p.delete();
            }
        }

        for(p in props) {
            if(Rl.isKeyDown(Rl.Keys.LEFT_CONTROL) && p.selected) {
                p.body.x = Rl.getMouseX();
                p.body.y = Rl.getMouseY();
            }
        }

        if(Rl.isKeyPressed(Rl.Keys.S)) {
            save();
        }

        echo.util.Debug.log(world);
        world.listen().separate = true;
    }

    public function save() {
        var sobs:Array<SaveObj> = [];

        for(p in props) {
            var s:SaveObj = {x: p.x, y: p.y, file: p.file, id: p.pid};
            sobs.push(s);
        }

        var fileModel:FileFormat =  {
            props: sobs
        };

        var s = haxe.Json.stringify(fileModel, "\t");
        sys.io.File.saveContent(savef, s);
    }

    override function render() {
        currentProp = Std.string(prop);
        Rl.drawText(currentProp, 0, 0, 23, Rl.Colors.WHITE);

        for(p in props) {
            p.render();
        }

        player.render();
    }
}