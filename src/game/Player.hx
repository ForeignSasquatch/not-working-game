import Rl.Vector2;
import echo.Body;
import rx.GameObject;

class Player extends GameObject {
    public var t:Rl.Texture;
    public var pos:Vector2;
    
    override function start() {
        t = Rl.loadTexture("res/TestBlock.png");
        body = new Body({
            x: 0,
            y: 0,
            mass: 1,
            shape: {
                type: RECT,
                width: t.width,
                height: t.height,
                solid: true
            }
        });
        body.on_move = (x, y) -> {
            pos.x = x;
            pos.y = y;
        };
    }

    override function update() {
        // body.x = Rl.getMouseX();
        // body.y = Rl.getMouseY();
    }

    override function render() {
        Rl.drawTextureV(t, Rl.Vector2.create(pos.x - (t.width / 2), pos.y - (t.height / 2)), Rl.Colors.WHITE);
        // Rl.drawTexturePro(t, )
    }
}